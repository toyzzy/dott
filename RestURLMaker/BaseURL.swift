//
//  BaseURL.swift
//  Dott
//
//  Created by Victor Sobolev on 01/12/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import Foundation

class BaseURL {
    static let baseURL = "https://api.foursquare.com/v2"
}
