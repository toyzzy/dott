//
//  VenuesURLMaker.swift
//  Dott
//
//  Created by Victor Sobolev on 01/12/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import Foundation

class VenuesURLMaker: URLMaker {
    var requestUrl: String

    init(_ baseUrl: String) {
        self.requestUrl = baseUrl
    }

    var venues: VenuesURLMaker {
        return addUrlPath("/venues")
    }
    
    var search: VenuesURLMaker {
        return addUrlPath("/search")
    }
    
}
