//
//  RestURLMaker.swift
//  Dott
//
//  Created by Victor Sobolev on 01/12/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import Foundation
import Alamofire

protocol URLMaker: class {
    var requestUrl: String {get set}
}

extension URLMaker where Self: Any {
    func addUrlPath(_ path: URLConvertible) -> Self {
        requestUrl += path.asString()
        return self
    }

    func add(param: String) -> Self {
        requestUrl += param
        return self
    }
    
}

extension URLConvertible where Self: Any {
    func asString() -> String {
        return self as? String ?? ""
    }
}
