//
//  DetailsDetailsConfigurator.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//

import UIKit

class DetailsModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController, ds: DetailsDataStorage) {

        if let viewController = viewInput as? DetailsViewController {
            configure(viewController: viewController, ds: ds)
        }
    }

    private func configure(viewController: DetailsViewController, ds: DetailsDataStorage) {

        let router = DetailsRouter()

        let presenter = DetailsPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = DetailsInteractor()
        interactor.output = presenter
        interactor.dataStorage = ds
        
        presenter.interactor = interactor
        viewController.output = presenter
    }

}
