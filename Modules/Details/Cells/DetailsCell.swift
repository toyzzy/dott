//
//  DetailsCell.swift
//  Dott
//
//  Created by Victor Sobolev on 01/12/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import Foundation
import UIKit

struct DetailsCellModel {
    var title: String?
    var subtitle: String?
}

class DetailsCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    var cellModel: DetailsCellModel? {
        didSet {
            titleLabel.text = cellModel?.title
            subtitleLabel.text = cellModel?.subtitle
        }
    }
}

class DetailsCellWireframe {
    static private let identifier = "DetailsCell"
    
    static func createCell(_ model: DetailsCellModel) -> DetailsCell {
        let cell = UserInterfaceService.viewFromMainBundle(identifier: identifier) as! DetailsCell
        cell.cellModel = model
        return cell
    }
}
