//
//  DetailsDetailsInteractor.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//

class DetailsInteractor: DetailsInteractorInput {
    
    weak var output: DetailsInteractorOutput!
    var dataStorage: DetailsDataStorage!
    
    func getVenueData() -> Venue {
        return dataStorage.venue
    }
}
