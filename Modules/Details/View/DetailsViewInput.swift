//
//  DetailsDetailsViewInput.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//

protocol DetailsViewInput: class {

    /**
        @author Victor Sobolev
        Setup initial state of the view
    */

    func setupInitialState<T: DetailsRequiredViewConfigurationFields>(initialData: T)
    func setCellData(_ models: [DetailsCellModel])
}
