//
//  DetailsDetailsViewOutput.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//

protocol DetailsViewOutput {

    /**
        @author Victor Sobolev
        Notify presenter that view is ready
    */

    func viewIsReady()
}
