//
//  DetailsDetailsViewController.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController, DetailsViewInput {

    var output: DetailsViewOutput!
    @IBOutlet weak var tableView: UITableView!
    private var cellModels = [DetailsCellModel]()
    private let cellHeight: CGFloat = 100
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        output.viewIsReady()
    }

    // MARK: DetailsViewInput
    func setupInitialState<T>(initialData: T) where T: DetailsRequiredViewConfigurationFields {
        
    }
    
    func setCellData(_ models: [DetailsCellModel]) {
        cellModels = models
        tableView.reloadData()
    }
}

extension DetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = DetailsCellWireframe.createCell(cellModels[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    
}
