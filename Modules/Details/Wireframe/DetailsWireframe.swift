//
//  DetailsDetailsWireframe.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//
import UIKit

let DetailsViewControllerIdentifier = "DetailsViewController"
let DetailsStoryboardIdentifier = "DetailsViewController"

class DetailsWireframe {
	weak var viewController: DetailsViewController?
	let configurator = DetailsModuleConfigurator()
		
    func initDetailsViewController(_ detailsDataStorage: DetailsDataStorage) -> DetailsViewController {
		if self.viewController == nil {
			let storyboard = UIStoryboard(name: DetailsStoryboardIdentifier, bundle: Bundle(for: type(of: self)))
            self.viewController = storyboard.instantiateViewController(withIdentifier: DetailsViewControllerIdentifier) as? DetailsViewController
            configurator.configureModuleForViewInput(viewInput: viewController, ds: detailsDataStorage)
		}
		return self.viewController!
	}

}
