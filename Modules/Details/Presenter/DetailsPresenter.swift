//
//  DetailsDetailsPresenter.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//

class DetailsPresenter: DetailsModuleInput, DetailsViewOutput, DetailsInteractorOutput {

    weak var view: DetailsViewInput!
    var interactor: DetailsInteractorInput!
    var router: DetailsRouterInput!
    var cellModelData = [DetailsCellModel]()
    
    enum Constants: String {
        case name
        case address
        case city
        case postalCode = "Postal code"
    }
    
    func viewIsReady() {
        prepareCellsData(interactor.getVenueData())
        view.setCellData(cellModelData)
    }
    
    private func prepareCellsData(_ venue: Venue) {
        cellModelData.append(DetailsCellModel(title: Constants.name.rawValue, subtitle: venue.name))
        cellModelData.append(DetailsCellModel(title: Constants.address.rawValue, subtitle: venue.location?.address))
        cellModelData.append(DetailsCellModel(title: Constants.city.rawValue, subtitle: venue.location?.city))
        cellModelData.append(DetailsCellModel(title: Constants.postalCode.rawValue, subtitle: venue.location?.postalCode))
    }
}
