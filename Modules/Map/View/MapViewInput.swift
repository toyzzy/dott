//
//  MapMapViewInput.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//
import GoogleMaps

protocol MapViewInput: class {

    /**
        @author Victor Sobolev
        Setup initial state of the view
    */

    func setupInitialState<T: MapRequiredViewConfigurationFields>(initialData: T)
    func showMarkers(_ markers: [GMSMarker])
    func centreAreaTo(_ location: CLLocationCoordinate2D, zoom: Float)
}
