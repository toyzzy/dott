//
//  MapMapViewController.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController, MapViewInput {

    var output: MapViewOutput!
    private var mapView: GMSMapView?
    private var markerTapped = false
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

    override func loadView() {
        prepareMapView()
    }
    
    private func prepareMapView() {
        let camera = GMSCameraPosition.camera(withLatitude: 0, longitude: 0, zoom: 12)
        mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        mapView?.isMyLocationEnabled = true
        mapView?.delegate = self
        mapView?.accessibilityElementsHidden = false
        self.view = mapView
    }
    
    // MARK: MapViewInput
    func setupInitialState<T>(initialData: T) where T: MapRequiredViewConfigurationFields {
        
    }
    
    func showMarkers(_ markers: [GMSMarker]) {
        mapView?.clear()
        for marker in markers {
            marker.map = mapView
        }
    }
    
    func centreAreaTo(_ location: CLLocationCoordinate2D, zoom: Float) {
        let camera = GMSCameraPosition.camera(withLatitude: location.latitude,
                                              longitude: location.longitude,
                                              zoom: zoom)
        mapView?.camera = camera
    }
}

extension MapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        let bounds = GMSCoordinateBounds(region: mapView.projection.visibleRegion())
           output.mapRegionChanged(bounds, userInitial: !markerTapped)
           markerTapped = false
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        markerTapped = true
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        output.didtapMarker(marker)
    }
}
