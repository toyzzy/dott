//
//  MapMapViewOutput.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//
import GoogleMaps

protocol MapViewOutput {

    /**
        @author Victor Sobolev
        Notify presenter that view is ready
    */

    func viewIsReady()
    func mapRegionChanged(_ newRegion: GMSCoordinateBounds, userInitial: Bool)
    func didtapMarker(_ marker: GMSMarker)
}
