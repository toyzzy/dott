//
//  MapMapInteractorInput.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//

import Foundation
import GoogleMaps

protocol MapInteractorInput {
    func startUpdatingUserlocation()
    func getVenues(_ newRegion: GMSCoordinateBounds)
}
