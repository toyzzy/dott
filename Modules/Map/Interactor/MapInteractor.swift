//
//  MapMapInteractor.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//
import MapKit
import GoogleMaps

class MapInteractor: MapInteractorInput {

    weak var output: MapInteractorOutput!
    var locationManager: LocationManager?
    var venueService = SearchVenuesService()
    private var lastUserLocation: CLLocationCoordinate2D?
    
    func startUpdatingUserlocation() {
        locationManager = LocationManager(self)
    }
    
    func getVenues(_ newRegion: GMSCoordinateBounds) {
        venueService.cancelAllRequests()
        let req = SearchVenuesServiceRequest(BoundingBox(sw: newRegion.southWest, ne: newRegion.northEast), userLocation: lastUserLocation ?? CLLocationCoordinate2D())
        venueService.searchVenues(req, complete: { [weak self] (venues) in
            guard let venues = venues else { return }
            self?.output.didLoadVenues(venues)
        }) { (error) in
            
        }
    }
}

extension MapInteractor: LocationManagerDelegate {
    func didUpdateUserlocation(_ location: CLLocation) {
        let newLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        if lastUserLocation == nil {
            output.userLocationLoaded(newLocation)
        }
        lastUserLocation = newLocation
    }
}
