//
//  MapMapInteractorOutput.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//

import Foundation
import MapKit

protocol MapInteractorOutput: class {
    func didLoadVenues(_ venues: [Venue])
    func userLocationLoaded(_ location: CLLocationCoordinate2D)
}
