//
//  MapMapPresenter.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//
import MapKit
import GoogleMaps

class MapPresenter: MapModuleInput, MapViewOutput, MapInteractorOutput {
    
    weak var view: MapViewInput!
    var interactor: MapInteractorInput!
    var router: MapRouterInput!
    private var currentmarkers = [GMSMarker]()
    private var venues = [Venue]()
    private let defaultZoom: Float = 12
    
    func viewIsReady() {
        interactor.startUpdatingUserlocation()
    }
    
    func didLoadVenues(_ venues: [Venue]) {
        self.venues = venues
        currentmarkers = [GMSMarker]()
        for venue in venues {
            guard let position = venue.location?.location else { return }
            let marker = GMSMarker(position: position)
            marker.title = venue.name
            currentmarkers.append(marker)
        }
        view.showMarkers(currentmarkers)
    }
    
    func userLocationLoaded(_ location: CLLocationCoordinate2D) {
        view.centreAreaTo(location, zoom: defaultZoom)
    }
    
    func mapRegionChanged(_ newRegion: GMSCoordinateBounds, userInitial: Bool) {
        if userInitial {
            interactor.getVenues(newRegion)
        }
    }
    
    func didtapMarker(_ marker: GMSMarker) {
        guard let index = currentmarkers.firstIndex(of: marker) else { return }
        let venue = venues[index]
        router.presentVenueDetails(venue)
    }
    
}
