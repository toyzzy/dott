//
//  MapMapRouter.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//
import UIKit

class MapRouter: MapRouterInput {
    var viewController: UIViewController!
    var wireframe = DetailsWireframe()
    
    func presentVenueDetails(_ venue: Venue) {
        let ds = DetailsDataStorage(venue: venue)
        let vc = wireframe.initDetailsViewController(ds)
        viewController.navigationController?.pushViewController(vc, animated: true)
    }
}
