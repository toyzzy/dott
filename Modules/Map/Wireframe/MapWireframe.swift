//
//  MapMapWireframe.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//
import UIKit

let MapViewControllerIdentifier = "MapViewController"
let MapStoryboardIdentifier = "MapViewController"

class MapWireframe {
	weak var viewController: MapViewController?
	let configurator = MapModuleConfigurator()
		
	func initMapViewController() -> MapViewController {
		if self.viewController == nil {
			let storyboard = UIStoryboard(name: MapStoryboardIdentifier, bundle: Bundle(for: type(of: self)))
            self.viewController = storyboard.instantiateViewController(withIdentifier: MapViewControllerIdentifier) as? MapViewController
			configurator.configureModuleForViewInput(viewInput: viewController)
		}
		return self.viewController!
	}

}
