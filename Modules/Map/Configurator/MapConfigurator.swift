//
//  MapMapConfigurator.swift
//  RenCredit
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//

import UIKit

class MapModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? MapViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: MapViewController) {

        let router = MapRouter()
        router.viewController = viewController
        
        let presenter = MapPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = MapInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
