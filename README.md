# Dott

## Architecture
As architecture pattern I user VIPER for easier test covering and understanding.

## Service layer (utility layer)
Service layer is splitted in:
UserInterfaceService - service for better work with .xibs and storyboards
RestURLMaker - service for making rest urls in declarative style
DataProvider - service for mapping objects from JSON
BaseCridentialsService - service for compiling all auth params in one place

## Managers (can make decisions)
APIPrepareManager - prepares API's layer
ApplicationManager - base routing
LocationManager - getting user location

## Network
Parsing models - swift style (codable/decodable)
Transport - Alamofire

## Unit tests
All modules covered fully, except View layer

## UI tests
Architecture style - Page Object Model
UI tests will work only in simulator with debug location "apple", connection to internet and iPhone 11 Pro Max device.

## What could be better?
DI container for bettter unit tests for services (need more time)
Make UITests work with mock data and more devices (need more time)
