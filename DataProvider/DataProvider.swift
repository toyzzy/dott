//
//  DataProvider.swift
//  Dott
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import Foundation

protocol DataProviderProtocol {
    func getData<Element: Decodable>(type: Element.Type, json: [String: Any]) -> Element?
}

class DataProvider: DataProviderProtocol {
    func getData<Element: Decodable>(type: Element.Type, json: [String: Any]) -> Element? {
        let decoder = JSONDecoder()
        do {
            let jsonData = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            let model = try decoder.decode(Element.self, from: jsonData)
            return model
        }  catch let DecodingError.dataCorrupted(context) {
            print(context)
        } catch let DecodingError.keyNotFound(key, context) {
            print("Key '\(key)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch let DecodingError.valueNotFound(value, context) {
            print("Value '\(value)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch let DecodingError.typeMismatch(type, context)  {
            print("Type '\(type)' mismatch:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch {
            print("error: ", error)
        }
        return nil
    }
}
