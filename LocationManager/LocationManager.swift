//
//  LocationManager.swift
//  Dott
//
//  Created by Victor Sobolev on 01/12/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import Foundation
import MapKit

protocol LocationManagerDelegate: class {
    func didUpdateUserlocation(_ location: CLLocation)
}

class LocationManager: NSObject, CLLocationManagerDelegate {
    var lastUserLocation: CLLocation?
    var locationManager = CLLocationManager()
    private weak var delegate: LocationManagerDelegate?
    
    init(_ delegate: LocationManagerDelegate?) {
        super.init()
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        self.delegate = delegate
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        lastUserLocation = location
        delegate?.didUpdateUserlocation(location)
        
      }
    
      func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            switch status {
            case .restricted:
              print("Location access was restricted.")
            case .denied:
              print("User denied access to location.")
            case .notDetermined:
              print("Location status not determined.")
            case .authorizedAlways:
                locationManager.startUpdatingLocation()
            case .authorizedWhenInUse:
                locationManager.startUpdatingLocation()
            @unknown default:
                print("Location status failed.")
            }
      }

      // Handle location manager errors.
      func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
      }
}
