//
//  XCUIElement+wait.swift
//  DottUITests
//
//  Created by Victor Sobolev on 02/12/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import Foundation
import XCTest

extension XCUIElement {
    
    enum ElementState: String {
        case exists = "exists == true"
        case absent = "exists == false"
        case visibile = "exists == true && hittable == true"
        case invisibile = "hittable == false"
        case isEnabled = "isEnabled == true"
    }
    
    func wait(fot timeout: TimeInterval = 10,
              _ state: ElementState = .exists,
              file: StaticString = #file,
              lime: UInt = #line) -> XCUIElement {
        
        let elementWithState: Bool
        
        if state == .exists {
            elementWithState = self.waitForExistence(timeout: timeout)
        } else {
            elementWithState = self.waitFor(state, timeout)
        }
        
        guard elementWithState else {
            XCTFail("Elemen \(description) with state \(state.rawValue) not found", file: file, line: lime)
            return self
        }
        
        return self
    }
    
    private func waitFor(_ state: ElementState, _ timeout: TimeInterval) -> Bool {
        let predicate = NSPredicate(format: state.rawValue)
        let expectation = XCTNSPredicateExpectation(predicate: predicate, object: self)
        return XCTWaiter.wait(for: [expectation], timeout: timeout) == .completed
    }
}
