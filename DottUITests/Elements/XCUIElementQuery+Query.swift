//
//  XCUIElementQuery+Query.swift
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import XCTest

extension XCUIElementQuery {
    
    func element(labelBegin: String) -> XCUIElement {
        return self.element(matching: NSPredicate(format: "label BEGINS WITH %@", labelBegin))
    }
    
    func element(identifierContain: String) -> XCUIElement {
        return self.element(matching: NSPredicate(format: "identifier CONTAINS[cd] %@", identifierContain))
    }
    
    func element(label: String) -> XCUIElement {
        return self.element(matching: NSPredicate(format: "label = %@", label))
    }
}

