//
//  XCUIApplication+Elemet.swift
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import XCTest

extension XCUIApplication {

    func getCell(identifier: String) -> XCUIElement {
        return self.cells.element(label: identifier)
    }
    
    func getButton(byString: String) -> XCUIElement {
        return self.buttons[byString].firstMatch
    }
    
    func getStaticText(byString: String) -> XCUIElement {
        return self.staticTexts[byString].firstMatch
    }
    
}
