//
//  BasePage.swift
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import XCTest

protocol BasePage {
    
    var displayName: String { get }
}

fileprivate enum Constants {
    static let timeout: TimeInterval = 5
}

extension BasePage {
    
    var app: XCUIApplication {
        return  XCUIApplication()
    }
    
    func apply(closure: (Self) -> Void) {
        closure(self)
    }
    
    func step<T>(name: String, activity: (Self) -> T) -> T {
        return XCTContext.runActivity(named: "\(displayName): \(name)") { _ in
            return activity(self)
        }
    }
    
    func verifyStaticText(byString: String) {
        let exist = app.staticTexts.element(label: byString).waitForExistence(timeout: Constants.timeout)
        XCTAssert(exist, "Element \(byString) not found")
    }
    
    func tapText(byString: String) {
        app.getStaticText(byString: byString).tap()
    }
}

