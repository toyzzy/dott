//
//  MapPage.swift
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import XCTest

final class MapPage: BasePage {
    var displayName: String = "Map"
    
    enum Locators: String {
        case mapView
        case marker = "Red Rock Coffee"
        case markerWindow = "Sushi Tomi"
    }
    
    enum Steps: String {
        case tapMarker = "Tap on marker"
        case tapInfoWindow = "Tap on window"
    }
    
    private lazy var marker = app.buttons.matching(identifier: Locators.marker.rawValue).element(boundBy: 0)
    private lazy var window = app.otherElements.matching(identifier: Locators.markerWindow.rawValue).element(boundBy: 0)

    
    func tapMarker() {
        step(name: Steps.tapMarker.rawValue) { _ in
            marker.wait().tap()
        }
    }
    
    func tapInfoWindow() {
        step(name: Steps.tapInfoWindow.rawValue) { _ in
            window.wait().tap()
        }
    }
}
