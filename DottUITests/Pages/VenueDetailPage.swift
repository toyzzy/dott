//
//  VenueDetailPage.swift
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import XCTest

final class VenueDetailPage: BasePage {
    var displayName: String = "Venue details"
    
    enum Locators: String {
        case back = "Back"
    }
    
    enum Steps: String {
        case tapBack = "Tap back button"
    }
    
    private lazy var backButton = app.getButton(byString: Locators.back.rawValue)
    
    func tapBackButton() {
        step(name: Steps.tapBack.rawValue) { _ in
            backButton.tap()
        }
    }
}

