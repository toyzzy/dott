//
//  MapTest.swift
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import XCTest

class MapTest: XCTestCase {
    
    let app = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        app.launch()

    }
    
    func testMap() {
        MapPage().apply {
            $0.tapMarker()
            $0.tapInfoWindow()
        }
        VenueDetailPage().apply {
            $0.tapBackButton()
        }
    }
    
    override func tearDown() {
        super.tearDown()
    }
}

