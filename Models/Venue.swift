//
//  Venue.swift
//  Dott
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import Foundation
import MapKit

struct VenueResponseBase: Decodable {
    enum CodingKeys: String, CodingKey {
        case response
    }
    
    var response: VenueResponse?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = try? container.decode(VenueResponse.self, forKey: .response)
    }
}

struct VenueResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case venues
    }
    
    var venues: [Venue]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        venues = try? container.decode([Venue].self, forKey: .venues)
    }
}

struct Venue: Decodable {
    enum CodingKeys: String, CodingKey {
        case name
        case location
        case categories
    }
    
    var name: String?
    var location: Location?
    var categories: [Category]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try? container.decode(String.self, forKey: .name)
        location = try? container.decode(Location.self, forKey: .location)
        categories = try? container.decode([Category].self, forKey: .categories)
    }
}

struct Location: Decodable {
    enum CodingKeys: String, CodingKey {
        case lat
        case lng
        case address
        case city
        case postalCode
    }
    
    var location: CLLocationCoordinate2D?
    var address: String?
    var city: String?
    var postalCode: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let lat = try? container.decode(Double.self, forKey: .lat), let lon = try? container.decode(Double.self, forKey: .lng) {
            location = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        }
        address = try? container.decode(String.self, forKey: .address)
        city = try? container.decode(String.self, forKey: .city)
        postalCode = try? container.decode(String.self, forKey: .postalCode)
    }
}

struct Category: Decodable {
    enum CodingKeys: String, CodingKey {
        case icon
        case name
    }
    
    var iconUrl: String?
    var name: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let icon = try? container.decode(Icon.self, forKey: .icon), let prefix = icon.prefix, let suffix = icon.suffix {
            iconUrl = prefix+suffix
        }
        name = try? container.decode(String.self, forKey: .name)
    }
}

struct Icon: Codable {
    enum CodingKeys: String, CodingKey {
        case prefix
        case suffix
    }
    
    var prefix: String?
    var suffix: String?
}
