//
//  UserInterfaceService.swift
//  Dott
//
//  Created by Victor Sobolev on 01/12/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import Foundation
import UIKit

class UserInterfaceService: NSObject {

    static func storyboard(identifier: String) -> UIStoryboard {
        let storyboard = UIStoryboard(name: identifier, bundle: Bundle(for: self))
        return storyboard
    }

    /// Returns viewcontroller from main storyboard
    ///
    /// - Parameter identifier: string identifier
    /// - Returns: viewcontroller
    static func viewControllerFromStoryboard<T: UIViewController>(identifier: String, storyboardIdentifier: String) -> T {
        let storyboard = UserInterfaceService.storyboard(identifier: storyboardIdentifier)
        let viewController = storyboard.instantiateViewController(withIdentifier: identifier)
        return viewController as! T
    }

    /// Returns view from main bundle
    ///
    /// - Parameter identifier: string identifier
    /// - Returns: view
    static func viewFromMainBundle<T: UIView>(identifier: String) -> T? {
        let bundle = Bundle.init(for: self)
        let view = bundle.loadNibNamed(identifier, owner: T(), options: nil)?.first
        return view as? T
    }
}
