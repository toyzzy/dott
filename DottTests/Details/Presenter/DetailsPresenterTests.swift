//
//  DetailsDetailsPresenterTests.swift
//  RenCredit
//
//  Created by Victor Sobolev on 02/12/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//

@testable import Dott
import XCTest

class DetailsPresenterTest: XCTestCase {
    var view = MockViewController()
    var router = MockRouter()
    var interactor = MockInteractor()
    var presenter = DetailsPresenter()
    var venueMock = venuesMock.first!
    
    override func setUp() {
        super.setUp()
        presenter.router = router
        presenter.interactor = interactor
        presenter.view = view
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testViewIsReady() {
        interactor.venueMock = venueMock
        presenter.viewIsReady()
        var containsName = false
        for model in view.models! {
            if model.subtitle == venueMock.name {
                containsName = true
                break
            }
        }
        XCTAssert(containsName, "Name didn't set right!")
    }

    class MockInteractor: DetailsInteractorInput {
        var venueMock: Venue?
        
        func getVenueData() -> Venue {
            return venueMock!
        }
        
    }

    class MockRouter: DetailsRouterInput {

    }

    class MockViewController: DetailsViewInput {
        var models: [DetailsCellModel]?
        
        func setupInitialState<T>(initialData: T) where T : DetailsRequiredViewConfigurationFields {
            
        }
        
        func setCellData(_ models: [DetailsCellModel]) {
            self.models = models
        }
        
        func setupInitialState() {

        }
    }
}
