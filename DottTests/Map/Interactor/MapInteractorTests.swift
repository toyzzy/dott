//
//  MapMapInteractorTests.swift
//  RenCredit
//
//  Created by Victor Sobolev on 02/12/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//

@testable import Dott
import XCTest
import GoogleMaps

class MapInteractorTests: XCTestCase {
    let presenter = MockPresenter()
    let interactor = MapInteractor()
    let venueService = MockVenueService()
    var locationManager: MockLocationManager?

    override func setUp() {
        super.setUp()
        interactor.output = presenter
        interactor.venueService = venueService
        interactor.locationManager = locationManager
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetVenues() {
        venueService.returnVenues = true
        interactor.getVenues(GMSCoordinateBounds())
        XCTAssert(venueService.cancelAllRequestsCalled, "Not canceled requests!")
        XCTAssert(presenter.didLoadVenuesCalled, "Didn't call loaded venues presenter!")
        XCTAssert(presenter.venues?.count == venuesMock.count, "Number of mock venues not match!")
    }
    
    func testStartUpdatingUserlocation() {
        let location = CLLocation()
        locationManager = MockLocationManager(interactor)
        locationManager!.location = location
        interactor.startUpdatingUserlocation()
        locationManager?.updateLocation()
        XCTAssert(presenter.userLocationLoadedCalled, "Didn't update user location!")
        XCTAssert(presenter.location?.latitude == location.coordinate.latitude, "Didn't update user location!")
        presenter.userLocationLoadedCalled = false
        interactor.didUpdateUserlocation(location)
        XCTAssert(!presenter.userLocationLoadedCalled, "Did update user location!")
        XCTAssert(presenter.location?.latitude == location.coordinate.latitude, "Did update user location!")
        
    }
    
    class MockLocationManager: LocationManager {
        var location: CLLocation?
        var delegate: LocationManagerDelegate?
        
        override init(_ delegate: LocationManagerDelegate?) {
            self.delegate = delegate
            super.init(nil)
        }
        
        func updateLocation() {
            delegate?.didUpdateUserlocation(location!)
        }
    }
    
    class MockVenueService: SearchVenuesService {
        var returnVenues = true
        var cancelAllRequestsCalled = false
        
        override func cancelAllRequests() {
            cancelAllRequestsCalled = true
        }
        override func searchVenues(_ request: SearchVenuesServiceRequest, complete: @escaping ([Venue]?) -> Void, errorHandle: @escaping (Error?) -> Void) {
            if returnVenues {
                complete(venuesMock)
            } else {
                complete(nil)
            }
        }
    }

    class MockPresenter: MapInteractorOutput {
        var didLoadVenuesCalled = false
        var userLocationLoadedCalled = false
        var venues: [Venue]?
        var location: CLLocationCoordinate2D?
        
        func didLoadVenues(_ venues: [Venue]) {
            didLoadVenuesCalled = true
            self.venues = venues
        }
        
        func userLocationLoaded(_ location: CLLocationCoordinate2D) {
            userLocationLoadedCalled = true
            self.location = location
        }
        

    }
}
