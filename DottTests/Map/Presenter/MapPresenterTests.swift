//
//  MapMapPresenterTests.swift
//  RenCredit
//
//  Created by Victor Sobolev on 02/12/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//

@testable import Dott
import XCTest
import GoogleMaps

class MapPresenterTest: XCTestCase {
    
    private let presenter = MapPresenter()
    private let interactor = MockInteractor()
    private let router = MockRouter()
    private let view = MockViewController()
    
    override func setUp() {
        super.setUp()
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = view
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testViewIsReady() {
        presenter.viewIsReady()
        XCTAssert(interactor.startUpdatingUserlocationCalled)
    }
    
    func testDidLoadVenues() {
        presenter.didLoadVenues(venuesMock)
        XCTAssert(view.showMarkersCalled, "Show markers not called!")
        XCTAssert(view.markers?.count == venuesMock.count, "Markers not right!")
        view.showMarkersCalled = false
        view.markers = nil
        presenter.didLoadVenues([Venue]())
        XCTAssert(view.showMarkersCalled, "Show markers not called!")
        XCTAssert(view.markers?.count == 0, "Markers count not right!")
    }
    
    func testUserLocationLoaded() {
        presenter.userLocationLoaded(CLLocationCoordinate2D())
        XCTAssert(view.centreAreaToCalled)
        XCTAssert(view.centreLocation?.latitude == CLLocationCoordinate2D().latitude)
        XCTAssert(view.centreLocation?.longitude == CLLocationCoordinate2D().longitude)
    }
    
    func testMapRegionChanged() {
        let region = GMSCoordinateBounds()
        presenter.mapRegionChanged(region, userInitial: true)
        XCTAssert(interactor.getVenuesCalled, "Get venue not called!")
        XCTAssert(interactor.region == region, "Region didn't set!")
        interactor.getVenuesCalled = false
        interactor.region = nil
        presenter.mapRegionChanged(region, userInitial: false)
        XCTAssert(!interactor.getVenuesCalled, "Get venue not called!")
        XCTAssert(interactor.region == nil, "Region settted!")
    }
    
    func testDidTapMarker() {
        let venues = venuesMock
        presenter.didLoadVenues(venues)
        presenter.didtapMarker(view.markers!.first!)
        XCTAssert(router.presentVenueDetailsCalled, "Present venue not called!")
        XCTAssert(router.venue!.name == venues.first!.name, "Venue name didn't set!")
    }

    class MockInteractor: MapInteractorInput {
        var startUpdatingUserlocationCalled = false
        var getVenuesCalled = false
        var region: GMSCoordinateBounds?
        
        func startUpdatingUserlocation() {
            startUpdatingUserlocationCalled = true
        }
        
        func getVenues(_ newRegion: GMSCoordinateBounds) {
            getVenuesCalled = true
            region = newRegion
        }
        
    }

    class MockRouter: MapRouterInput {
        var presentVenueDetailsCalled = false
        var venue: Venue?
        
        func presentVenueDetails(_ venue: Venue) {
            presentVenueDetailsCalled = true
            self.venue = venue
        }

    }

    class MockViewController: MapViewInput {
        var showMarkersCalled = false
        var centreAreaToCalled = false
        var setupInitialStateCalled = false
        var markers: [GMSMarker]?
        var centreLocation: CLLocationCoordinate2D?
        
        func setupInitialState<T>(initialData: T) where T : MapRequiredViewConfigurationFields {
            
        }
        
        func showMarkers(_ markers: [GMSMarker]) {
            showMarkersCalled = true
            self.markers = markers
        }
        
        func centreAreaTo(_ location: CLLocationCoordinate2D, zoom: Float) {
            centreAreaToCalled = true
            centreLocation = location
        }
        
        func setupInitialState() {
            setupInitialStateCalled = true
        }
    }
}
