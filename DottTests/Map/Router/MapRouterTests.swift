//
//  MapMapRouterTests.swift
//  RenCredit
//
//  Created by Victor Sobolev on 02/12/2019.
//  Copyright © 2019 RenCredit. All rights reserved.
//

@testable import Dott
import XCTest

class MapRouterTests: XCTestCase {
    var router = MapRouter()
    var wireframe = MockDetailsWireframe()
        
    override func setUp() {
        super.setUp()
        router.wireframe = wireframe
        router.viewController = UIViewController()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPresentVenueDetails() {
        let venue = venuesMock.first!
        router.presentVenueDetails(venue)
        XCTAssert(venue.name == wireframe.ds!.venue!.name, "Didn't set right data!")
    }
    
    class MockDetailsWireframe: DetailsWireframe {
        var ds: DetailsDataStorage?
        
        override func initDetailsViewController(_ detailsDataStorage: DetailsDataStorage) -> DetailsViewController {
            ds = detailsDataStorage
            let vc = DetailsViewController()
            return vc
        }
    }
}
