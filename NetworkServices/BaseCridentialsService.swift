//
//  BaseService.swift
//  Dott
//
//  Created by Victor Sobolev on 01/12/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import Foundation

class BaseCridentialsService {
    enum Constants: String {
        case client_id
        case client_secret
        case clientId = "JDUOUUBW4GZBEMXCAIENUU2YRJSOZNCKRVBA3YID5S5OBFFI"
        case clientSecret = "PV4ETEHPYXGIX5FOIQLTBCVA0Y00HZ3LKPM1ERXYE5MZSZPI"
        case v
        case version = "20180323"
    }
    let cridentials: [String: Any] = [Constants.client_id.rawValue: Constants.clientId.rawValue,
                                      Constants.client_secret.rawValue: Constants.clientSecret.rawValue,
                                      Constants.v.rawValue: Constants.version.rawValue]
    
    func appendParams(_ params: [String: Any]) -> [String: Any] {
        let fullParams = cridentials.merging(params) { (current, _) in current }
        return fullParams
    }
}
