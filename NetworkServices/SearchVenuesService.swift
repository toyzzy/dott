//
//  SearchVenuesService.swift
//  Dott
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import Foundation
import MapKit

struct BoundingBox {
    let sw: CLLocationCoordinate2D!
    let ne: CLLocationCoordinate2D!
}

class SearchVenuesServiceRequest: RestRequest {
    
    private let cridentialsService = BaseCridentialsService()
    private var bound: BoundingBox!
    private var userLocation: CLLocationCoordinate2D!
    
    enum Constants: String {
        case sw
        case ne
        case intent
        case ll
        case categoryId
        case restaurantsCategory = "4d4b7105d754a06374d81259"
        case browse
    }
    
    init(_ bounding: BoundingBox, userLocation: CLLocationCoordinate2D) {
        bound = bounding
        self.userLocation = userLocation
    }
    
    func getParams() -> [String : Any] {
        let swFormatted = "\(bound.sw.latitude),\(bound.sw.longitude)"
        let neFormatted = "\(bound.ne.latitude),\(bound.ne.longitude)"
        let params = cridentialsService.appendParams([Constants.sw.rawValue: swFormatted,
                                                      Constants.ne.rawValue: neFormatted,
                                                      Constants.categoryId.rawValue: Constants.restaurantsCategory.rawValue,
                                                      Constants.intent.rawValue: Constants.browse.rawValue])
        return params
    }
}

class SearchVenuesService {
    private let baseService = BaseNetworkService<VenueResponseBase>()
    
    func cancelAllRequests() {
        baseService.sessionManager.session.getAllTasks { (task) in
                task.forEach { $0.cancel() }
        }
    }
    
    func searchVenues(_ request: SearchVenuesServiceRequest, complete: @escaping ([Venue]?) -> Void, errorHandle: @escaping (Error?) -> Void) {
        let venuesURL = VenuesURLMaker(BaseURL.baseURL).venues.search.requestUrl
        baseService.getElement(url: venuesURL, request: request, completion: { (venueResponse) in
            guard let venueResponse = venueResponse else { return }
            complete(venueResponse.response?.venues)
        }) { (error) in
            errorHandle(error)
        }
    }
}
