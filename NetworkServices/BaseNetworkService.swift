//
//  BaseNetworkService.swift
//  Dott
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import Foundation
import Alamofire

class BaseNetworkService<T: Decodable> {
    private var provider = DataProvider()
    
    open var sessionManager = Alamofire.SessionManager.default

    func getElement(url: String, request: RestRequest, completion: @escaping (T?) -> Void, error: @escaping (Error?) -> Void) {
        sessionManager.request(url, method: HTTPMethod.get, parameters: request.getParams(), headers: nil).responseJSON { [weak self] (response) in
            if let error = response.result.error as NSError?, error.code == NSURLErrorCancelled {
                return
            }
            if let netError = response.result.error {
                error(netError)
            }
            guard let JSON = response.result.value as? [String: Any] else {
                error(nil)
                return
            }
            let mappedData = self?.provider.getData(type: T.self, json: JSON)
            completion(mappedData)
        }
    }
}
