//
//  AppDelegate.swift
//  Dott
//
//  Created by Victor Sobolev on 29/11/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import UIKit
import MapKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    lazy var applicationMainRouter = ApplicationRouter()
    lazy var apiManager = APIPrepareManager()
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        apiManager.applicationStart()
        applicationMainRouter.mainStart(&window)
        return true
    }

}

