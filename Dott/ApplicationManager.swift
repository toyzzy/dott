//
//  ApplicationManager.swift
//  Dott
//
//  Created by Victor Sobolev on 01/12/2019.
//  Copyright © 2019 Victor Sobolev. All rights reserved.
//

import Foundation
import UIKit

class ApplicationRouter {
    func mainStart(_ window: inout UIWindow?) {
        window = UIWindow.init(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        let rootVC = MapWireframe().initMapViewController()
        let navigationController = UINavigationController()
        navigationController.viewControllers = [rootVC]
        
        window?.rootViewController = navigationController
    }
}
